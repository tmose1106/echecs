======
échecs
======

A game of 4d chess

Running
-------

To run, install Python 3.6 or greater and `poetry <https://python-poetry.org/>`_.

Then in the project directory, simply run ``poetry install`` followed by ``poetry run echecs``.

