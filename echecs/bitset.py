class ImmutableBitset:
    ''' A container for immutable bit manipulation of the Python int type '''
    def __init__(self, length, value=0):
        self._length = length
        self._max = 2 ** length - 1

        if value > self._max:
            raise ValueError(f'Value {value} greater than maximum value {self._max}')

        self._value = value

    def __int__(self):
        return self._value

    def __len__(self):
        return self._length

    def __getitem__(self, index):
        if index >= self._length:
            raise IndexError('Index is out of range')

        return bool((self._value & (1 << index)) >> index)

    def __lshift__(self, shift):
        return ImmutableBitset(self._length, (self._value << shift) & self._max)

    def __rshift__(self, shift):
        return ImmutableBitset(self._length, (self._value >> shift))

    def __and__(self, other):
        if not isinstance(other, ImmutableBitset):
            raise ValueError('ImmutableBitset must be OR with another Bitset')

        return ImmutableBitset(self._length, (self._value & other._value) & self._max)

    def __iand__(self, other):
        return self.__and__(other)

    def __or__(self, other):
        if not isinstance(other, ImmutableBitset):
            raise ValueError('ImmutableBitset must be OR with another Bitset')

        return ImmutableBitset(self._length, (self._value | other._value) & self._max)

    def __ior__(self, other):
        return self.__or__(other)

    def __invert__(self):
        return ImmutableBitset(self._length, ~self._value & self._max)

    def __iter__(self):
        for index in range(self._length):
            yield self[index]

    def set(self, index):
        return ImmutableBitset(self._length, self._value | (1 << index))

    def unset(self, index):
        return ImmutableBitset(self._length, self._value & ~(1 << index))

    def any(self):
        for is_set in self:
            if is_set:
                return True
        else:
            return False

    def rotate(self, value):
        ''' Positive is rotate left, negative is rotate right '''
        rotate_value = value % self._length

        # Mask the rotation with the max to get only bits that fit in the length
        new_value = ((self._value << rotate_value) | (self._value >> (self._length - rotate_value))) & self._max

        return ImmutableBitset(self._length, new_value)


class Bitset(ImmutableBitset):
    ''' A container for mutable bit manipulation of the Python int type '''
    def __init__(self, length, value=0):
        super().__init__(length, value)

    def set(self, index):
        self._value |= (1 << index)

    def unset(self, index):
        self._value &= ~(1 << index)

    def rotate(self, value):
        ''' Positive is rotate left, negative is rotate right '''
        rotate_value = value % self._length

        # Mask the rotation with the max to get only bits that fit in the length
        self._value = ((self._value << rotate_value) | (self._value >> (self._length - rotate_value))) & self._max

    def get_immutable(self):
        return ImmutableBitset(self._length, self._value)
