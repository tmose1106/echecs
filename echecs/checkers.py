from collections import namedtuple
from functools import partial

from .bitset import ImmutableBitset


# Our checkers bitboard is 4x8, or 32 bits long
Bitboard = partial(ImmutableBitset, 32)

MASK_L3 = Bitboard(0b0000_1110_0000_1110_0000_1110_0000_1110)
MASK_L5 = Bitboard(0b0000_0000_0111_0000_0111_0000_0111_0000)
MASK_R3 = Bitboard(0b0111_0000_0111_0000_0111_0000_0111_0000)
MASK_R5 = Bitboard(0b0000_1110_0000_1110_0000_1110_0000_0000)
MASK_LOWER_KING_ROW = Bitboard(0b1111 << 28)
MASK_UPPER_KING_ROW = Bitboard(0b1111)

# Static list of bitboards showing where a piece at an
#   index can move to.
LIST_A = [0b0001, 0b0011, 0b0110, 0b1100]
LIST_B = [0b0011, 0b0110, 0b1100, 0b1000]
LIST_C = [0b0000, 0b0000, 0b0000, 0b0000]

UPWARDS_MOVE_LIST = [
    Bitboard(bit_value)
    for bit_value in [
        *[value << 4 for value in LIST_A],
        *[value << 8 for value in LIST_B],
        *[value << 12 for value in LIST_A],
        *[value << 16 for value in LIST_B],
        *[value << 20 for value in LIST_A],
        *[value << 24 for value in LIST_B],
        *[value << 28 for value in LIST_A],
        *LIST_C
    ]
]

DOWNWARDS_MOVE_LIST = [
    Bitboard(bit_value)
    for bit_value in [
        *LIST_C,
        *LIST_B,
        *[value << 4 for value in LIST_A],
        *[value << 8 for value in LIST_B],
        *[value << 12 for value in LIST_A],
        *[value << 16 for value in LIST_B],
        *[value << 20 for value in LIST_A],
        *[value << 24 for value in LIST_B]
    ]
]

KING_MOVE_LIST = [
    up_bitboard | down_bitboard
    for up_bitboard, down_bitboard in zip(UPWARDS_MOVE_LIST, DOWNWARDS_MOVE_LIST)
]

_JUMP_LIST_ROW = [0b0010, 0b0101, 0b1010, 0b0100]

UPWARDS_JUMP_LIST = [
    Bitboard(bit_value)
    for bit_value in [ 
        *[value << 8 for value in _JUMP_LIST_ROW],
        *[value << 12 for value in _JUMP_LIST_ROW],
        *[value << 16 for value in _JUMP_LIST_ROW],
        *[value << 20 for value in _JUMP_LIST_ROW],
        *[value << 24 for value in _JUMP_LIST_ROW],
        *[value << 28 for value in _JUMP_LIST_ROW],
        *LIST_C,
        *LIST_C
    ]
]

DOWNWARDS_JUMP_LIST = [
    Bitboard(bit_value)
    for bit_value in [
        *LIST_C,
        *LIST_C,
        *_JUMP_LIST_ROW,
        *[value << 4 for value in _JUMP_LIST_ROW],
        *[value << 8 for value in _JUMP_LIST_ROW],
        *[value << 12 for value in _JUMP_LIST_ROW],
        *[value << 16 for value in _JUMP_LIST_ROW],
        *[value << 20 for value in _JUMP_LIST_ROW]
    ]
]

KING_JUMP_LIST = [
    up_bitboard | down_bitboard
    for up_bitboard, down_bitboard in zip(UPWARDS_JUMP_LIST, DOWNWARDS_JUMP_LIST)
]

BoardState = namedtuple('CheckersBoardState', ['lower', 'upper', 'kings'])
MoveDict = namedtuple('CheckersMoveSet', ['side', 'type', 'dict'])


def get_lower_movers(lower_pieces, upper_pieces, kings):
    not_occupied = ~(lower_pieces | upper_pieces)

    movers = (not_occupied >> 4) & lower_pieces

    movers |= ((not_occupied & MASK_R3) >> 3) & lower_pieces

    movers |= ((not_occupied & MASK_R5) >> 5) & lower_pieces

    lower_kings = lower_pieces & kings

    if (lower_kings.any()):
        movers |= (not_occupied << 4) & lower_kings
        movers |= ((not_occupied & MASK_R3) << 3) & lower_kings
        movers |= ((not_occupied & MASK_R5) << 5) & lower_kings

    return movers


def get_lower_jumpers(lower_pieces, upper_pieces, kings):
    not_occupied = ~(lower_pieces | upper_pieces)

    movers = Bitboard(0)

    temp = (not_occupied >> 4) & upper_pieces

    if temp.any():
        temp_masked = ((temp & MASK_R3) >> 3) | ((temp & MASK_R5) >> 5)
        movers = movers | ((temp_masked) & lower_pieces)

    temp = ( ((not_occupied & MASK_R3) >> 3) | ((not_occupied & MASK_R5) >> 5) ) & upper_pieces

    movers |= (temp >> 4) & lower_pieces

    lower_kings = lower_pieces & kings

    if lower_kings.any():
        temp = (not_occupied << 4) & upper_pieces

        if temp.any():
            movers |= ( ((temp & MASK_L3) << 3) | ((temp & MASK_L5) << 5) ) & lower_kings

        temp_2 = ( ((not_occupied & MASK_L3) << 3) | ((not_occupied & MASK_L5) << 5) ) & upper_pieces

        if temp_2.any():
            movers |= (temp_2 << 4) & lower_kings

    return movers


def get_upper_movers(lower_pieces, upper_pieces, kings):
    # Unnoccupies spaces are define by not having any pieces
    not_occupied = ~(upper_pieces | lower_pieces)

    # Upper pieces may move if there are unoccupied spaces diagonal to them
    #   (south-east for even rows, south-west for odd rows)
    movers = (not_occupied << 4) & upper_pieces

    # not_occupied & mask_l3: not occupied even row positions which can be
    #   moved to south-east
    # << 3: positions where a piece could move to an unoccupied position
    #   south-east
    # & white_pieces: The pieces which can make this move
    movers |= ((not_occupied & MASK_L3) << 3) & upper_pieces

    # Same as above, except south-west
    movers |= ((not_occupied & MASK_L5) << 5) & upper_pieces

    upper_kings = upper_pieces & kings

    if (upper_kings.any()):
        movers |= (not_occupied >> 4) & upper_kings
        movers |= ((not_occupied & MASK_R3) >> 3) & upper_kings
        movers |= ((not_occupied & MASK_R5) >> 5) & upper_kings

    return movers


def get_upper_jumpers(lower_pieces, upper_pieces, kings):
    not_occupied = ~(lower_pieces | upper_pieces)

    movers = Bitboard(0)

    temp = (not_occupied << 4) & lower_pieces

    if temp.any():
        temp_masked = ((temp & MASK_L3) << 3) | ((temp & MASK_L5) << 5)
        movers |= (temp_masked) & upper_pieces

    temp = ( ((not_occupied & MASK_L3) << 3) | ((not_occupied & MASK_L5) << 5) ) & lower_pieces

    movers |= (temp << 4) & upper_pieces

    upper_kings = upper_pieces & kings

    if upper_kings.any():
        temp = (not_occupied >> 4) & lower_pieces

        if temp.any():
            movers |= ( ((temp & MASK_R3) >> 3) | ((temp & MASK_R5) >> 5) ) & upper_kings

        temp_2 = ( ((not_occupied & MASK_R3) >> 3) | ((not_occupied & MASK_R5) >> 5) ) & lower_pieces

        if temp_2.any():
            movers |= (temp_2 >> 4) & upper_kings

    return movers


def get_board_position_from_index(index):
    column_index, row_index = index % 4, index // 4

    column_offset = row_index % 2

    return (column_index * 2 + column_offset, row_index)


def get_board_positions_for_set_bits(bitboard):
    return [
        get_board_position_from_index(index)
        for index, bit in enumerate(bitboard) if bit
    ]


def get_index_from_position(position):
    x, y = position

    return (y * 4) + (x // 2)


def print_as_board(bitset):
    rows = [list(bitset)[start:start + 4] for start in range(0, 32, 4)]

    for index, row in enumerate(reversed(rows)):
        row_string = ' '.join(('1' if bit else '0' for bit in row))

        if index % 2:
            print(row_string)
        else:
            print(' ' + row_string) 


CheckersEvent = namedtuple('CheckersEvent', ['type', 'positions'])


class Checkers:
    def __init__(self):
        # self._board = BoardState(
        # )

        self._board = BoardState(
            # Bitboard(0b111111111111),
            # Bitboard(0b111111111111 << 20),
            # Bitboard(0)

            Bitboard(0b0100_0000_1001),
            Bitboard(0b1000_1101 << 12),
            Bitboard(0b0000_0000_0000_0000_0100_0100_0000_0000)

            # Bitboard(0b0100_0000_0000),
            # Bitboard(0b1000_0000 << 12),
            # Bitboard(0b0000_0000_0000_0000_0000_0000_0000_0000)
        )

        self._not_occupied = ~(self._board.lower | self._board.upper)

        self._turn = 'lower'

        self._history = []

    @property
    def board(self):
        return self._board

    @board.setter
    def board(self, value):
        self._not_occupied = ~(value.lower | value.upper)

        self._board = value

    @staticmethod
    def _get_indices_from_bitboard(bitboard):
        return [index for index, bit in enumerate(bitboard) if bit]

    def get_piece_layout(self):
        black_pieces, white_pieces, kings = self._board
 
        bitboards = {
            'black_kings': black_pieces & kings,
            'black_pieces': black_pieces & ~kings,
            'white_kings': white_pieces & kings,
            'white_pieces': white_pieces & ~kings,
        }

        return {
            name: get_board_positions_for_set_bits(bitboard)
            for name, bitboard in bitboards.items()
        }

    @staticmethod
    def _get_enemy_index(piece_index, destination_index):
        '''
        Given a piece index and jump destination index, we can find
        the index of where an enemy piece should be.
        '''
        difference = destination_index - piece_index

        odd_row = (piece_index // 4) % 2 == 1

        if odd_row:
            if difference == 7: # Northwest
                return piece_index + 4
            elif difference == 9: # Northeast
                return piece_index + 5
            elif difference == -9:
                return piece_index - 4
            elif difference == -7:
                return piece_index - 3
        else:
            if difference == 7: # Northwest
                return piece_index + 3
            elif difference == 9: # Northeast
                return piece_index + 4
            elif difference == -9:
                return piece_index - 5
            elif difference == -7:
                return piece_index - 4

    @staticmethod
    def _get_position_from_index(index):
        column_index, row_index = index % 4, index // 4

        column_offset = row_index % 2

        return (column_index * 2 + column_offset, row_index)

    def _map_destinations(self, piece_board, destination_board_list):
        return {
            piece_index: self._get_indices_from_bitboard(destination_board_list[piece_index] & self._not_occupied)
            for piece_index in self._get_indices_from_bitboard(piece_board)
        }

    def _get_move_options(self, movers, men_move_list, king_move_list):
        kings = self.board.kings

        mover_indicies = {
            **self._map_destinations(movers & ~kings, men_move_list),
            **self._map_destinations(movers & kings, king_move_list),
        }

        return {
            self._get_position_from_index(piece_index): {
                self._get_position_from_index(destination_index)
                for destination_index in destination_indicies
            }
            for piece_index, destination_indicies in mover_indicies.items()
        }

    def _get_jump_options(self, jumpers, enemies, men_jump_list, king_jump_list):
        kings = self.board.kings

        jumper_indices = {
            **self._map_destinations(jumpers & ~kings, men_jump_list),
            **self._map_destinations(jumpers & kings, king_jump_list),
        }

        jumper_positions = {}

        for piece_index, destination_indices in jumper_indices.items():
            possible_jumps = []

            for destination_index in destination_indices:
                enemy_index = self._get_enemy_index(piece_index, destination_index)

                if enemies[enemy_index] and self._not_occupied[destination_index]:
                    possible_jumps.append((
                        self._get_position_from_index(enemy_index),
                        self._get_position_from_index(destination_index)))

            if possible_jumps:
                jumper_positions[self._get_position_from_index(piece_index)] = possible_jumps

        return jumper_positions

    def _get_lower_move_options(self):
        jumpers = get_lower_jumpers(*self.board)

        # If any pieces can jump...
        if jumpers.any():
            return MoveDict(self._turn, 'jumps', self._get_jump_options(jumpers, self.board.upper, UPWARDS_JUMP_LIST, KING_JUMP_LIST))

        movers = get_lower_movers(*self.board)

        return MoveDict(self._turn, 'moves', self._get_move_options(movers, UPWARDS_MOVE_LIST, KING_MOVE_LIST))

    def _get_upper_move_options(self):
        jumpers = get_upper_jumpers(*self.board)

        if jumpers.any():
            return MoveDict(self._turn, 'jumps', self._get_jump_options(jumpers, self.board.lower, DOWNWARDS_JUMP_LIST, KING_JUMP_LIST))

        movers = get_upper_movers(*self.board)

        return MoveDict(self._turn, 'moves', self._get_move_options(movers, DOWNWARDS_MOVE_LIST, KING_MOVE_LIST))

    def get_possible_move_options(self):
        side = self._turn

        if side == 'upper':
            return self._get_upper_move_options()
        elif side == 'lower':
            return self._get_lower_move_options()

    def make_move(self, piece_position, destination_position):
        side = self._turn

        possible_moves = self.get_possible_move_options()

        if side == 'upper':
            enemy, ally, kings = self.board
            king_mask = MASK_UPPER_KING_ROW
        elif side == 'lower':
            ally, enemy, kings = self.board
            king_mask = MASK_LOWER_KING_ROW

        events = []

        if possible_moves.type == 'jumps':
            possible_jumps = possible_moves.dict[piece_position]

            destination_positions = [pair[1] for pair in possible_moves.dict[piece_position]]
            valid_jump = piece_position in possible_moves.dict and destination_position in destination_positions

            if valid_jump:
                enemy_position = next((pair[0] for pair in possible_jumps if pair[1] == destination_position), None)

                events.append(CheckersEvent('move', [piece_position, destination_position]))
                events.append(CheckersEvent('capture', [enemy_position]))

                piece_index, dest_index = get_index_from_position(piece_position), get_index_from_position(destination_position)
                enemy_index = get_index_from_position(enemy_position)

                ally = ally.unset(piece_index).set(dest_index)

                if kings[piece_index]:
                    kings = kings.unset(piece_index).set(dest_index)

                enemy = enemy.unset(enemy_index)

                if kings[enemy_index]:
                    kings = kings.unset(enemy_index)

                new_kings = (ally & ~kings) & king_mask

                if new_kings.any():
                    events.append(CheckersEvent('promotion', [destination_position]))

                kings |= new_kings

                if not enemy.any():
                    events.append(CheckersEvent('won', None))

                if side == 'upper':
                    self.board = BoardState(enemy, ally, kings)

                    if not get_upper_jumpers(*self._board)[dest_index]:
                        self._turn = 'lower'
                elif side == 'lower':
                    self.board = BoardState(ally, enemy, kings)

                    if not get_lower_jumpers(*self._board)[dest_index]:
                        self._turn = 'upper'
        elif possible_moves.type == 'moves':
            valid_move = piece_position in possible_moves.dict and destination_position in possible_moves.dict[piece_position]

            if valid_move:
                events.append(CheckersEvent('move', (piece_position, destination_position)))

                piece_index, dest_index = get_index_from_position(piece_position), get_index_from_position(destination_position)

                ally = ally.unset(piece_index).set(dest_index)

                if kings[piece_index]:
                    kings = kings.unset(piece_index).set(dest_index)

                new_kings = (ally & ~kings) & king_mask 

                if new_kings.any():
                    events.append(CheckersEvent('promotion', [destination_position]))

                kings |= new_kings

                if side == 'upper':
                    self.board = BoardState(enemy, ally, kings)
                    self._turn = 'lower'
                elif side == 'lower':
                    self.board = BoardState(ally, enemy, kings)
                    self._turn = 'upper'

        return events
