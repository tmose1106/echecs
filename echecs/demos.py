import pyglet

def run_demo():
    window = pyglet.window.Window(WINDOW_SIZE, WINDOW_SIZE, NAME)

    board_batch = pyglet.graphics.Batch()

    board_squares = [
        pyglet.shapes.Rectangle(
            x * SQUARE_SIZE, (y * 2 + x % 2) * SQUARE_SIZE,
            SQUARE_SIZE, SQUARE_SIZE,
            color=BOARD_COLOR,
            batch=board_batch)
        for y in range(4)
        for x in range(8)
    ]

    label = pyglet.text.Label(f'{NAME}!',
        font_name='Comics Sans',
        font_size=48,
        x = window.width // 2, y = window.height // 2,
        anchor_x='center', anchor_y='center'
    )

    @window.event
    def on_draw():
        window.clear()
        board_batch.draw()
        label.draw()

    # event_logger = pyglet.window.event.WindowEventLogger()

    # window.push_handlers(event_logger)

    pyglet.app.run()


def run_click_demo():
    window = pyglet.window.Window(640, 480, 'Dragging Demo')

    square = pyglet.shapes.Rectangle(32, 32, 128, 128, color=(0, 128, 0))

    is_dragging = False

    def is_in_square(x, y):
        return (x > square.x and x < square.x + square.width) and (y > square.y and y < square.y + square.height)

    @window.event
    def on_mouse_motion(x, y, dx, dy):
        if is_in_square(x, y):
            square.color = (128, 0, 0)
        else:
            square.color = (0, 128, 0)

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        nonlocal is_dragging

        if is_in_square(x, y):
            square.color = (0, 0, 128)

            is_dragging = True

    @window.event
    def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
        if is_dragging:
            square.x += dx
            square.y += dy

    @window.event
    def on_mouse_release(x, y, button, modifiers):
        nonlocal is_dragging

        if is_in_square(x, y):
            square.color = (128, 0, 0)
            
            is_dragging = False

    @window.event
    def on_draw():
        window.clear()
        square.draw()

    pyglet.app.run()


