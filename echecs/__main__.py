from collections import namedtuple
from pathlib import Path
from random import choice
from sys import exit as sys_exit
from time import sleep

import pyglet

from .checkers import Checkers


NAME = 'échecs'

WINDOW_SIZE = 800
SQUARE_SIZE = WINDOW_SIZE // 8


class SpriteManager:
    def __init__(self, image, width, batch, count):
        self._sprites = [
            pyglet.sprite.Sprite(image, 0, 0, batch=batch)
            for _ in range(count)
        ]

        for sprite in self._sprites:
            sprite.scale = width / image.width

        self._sprite_positions = {}

        self.hide_all()

    def get(self, position):
        return self._sprite_positions[position]

    def get_all(self):
        return self._sprite_positions

    @staticmethod
    def _get_screen_position(position):
        return tuple(value * SQUARE_SIZE for value in position)

    def set_many(self, positions):
        new_positions = {}

        position_count = len(positions)

        assert(position_count <= len(self._sprites))

        for position, sprite in zip(positions, self._sprites[:position_count]):
            sprite.position = self._get_screen_position(position)
            sprite.visible = True

            new_positions[position] = sprite

        for sprite in self._sprites[position_count:]:
            sprite.visible = False

        self._sprite_positions = new_positions

    def move(self, position, new_position):
        sprite = self._sprite_positions[position]

        sprite.position = self._get_screen_position(position)
        sprite.visible = True

        self._sprite_positions[new_position] = sprite

        del self._sprite_positions[position]

    def move_between(self, source_position, target_position, percent): 
        screen_position = tuple(
            (source_coord + (target_coord - source_coord) * percent) * SQUARE_SIZE
            for source_coord, target_coord in zip(source_position, target_position)
        )

        sprite = self._sprite_positions[source_position]
        sprite.position = screen_position

    def hide_all(self):
        for sprite in self._sprites:
            sprite.visible = False


class CheckersPieceSpriteManager:
    def __init__(self, width, batch, sprite_sheet_image):
        self._batch = batch
        
        sprite_grid = pyglet.image.ImageGrid(sprite_sheet_image, 5, 15)

        self._animations = self.get_animations_from_sprite_grid(sprite_grid)

        self._man_image = sprite_grid[8]
        self._man_scale = width / self._man_image.width
        
        self._king_image = sprite_grid[0]
        self._king_scale = width / self._king_image.width

        self._men_sprites, self._king_sprites = {}, {}

    @staticmethod
    def get_animations_from_sprite_grid(image_grid):
        image_list_dict = {
            'man_promote': [
                *reversed(image_grid[0:7])
            ],
            'man_northeast': [
                *reversed(image_grid[15:29]),
                image_grid[8]
            ],
            'man_northwest': [
                *reversed(image_grid[30:44]),
                image_grid[8]
            ],
            'man_southeast': [
                *image_grid[30:44],
                image_grid[8]
            ],
            'man_southwest': [
                *image_grid[15:29],
                image_grid[8]
            ],
            'king_northeast': [
                *reversed(image_grid[45:59]),
                image_grid[0]
            ],
            'king_northwest': [
                *reversed(image_grid[60:74]),
                image_grid[0]
            ],
            'king_southeast': [
                *image_grid[60:74],
                image_grid[0]
            ],
            'king_southwest': [
                *image_grid[45:59],
                image_grid[0]
            ],
        }

        return {
            key: pyglet.image.Animation.from_image_sequence(image_list, duration=1 / 32, loop=False)
            for key, image_list in image_list_dict.items()
        }

    def promote(self, position):
        try:
            sprite = self._men_sprites[position]
        except KeyError:
            return

        sprite.image = self._animations['man_promote']

        self._king_sprites[position] = sprite

        del self._men_sprites[position]

    def remove(self, position):
        sprite = self.get_sprite(position)

        sprite.visible = False

    def move(self, source_position, target_position):
        if source_position in self._men_sprites:
            sprites = self._men_sprites
        elif source_position in self._king_sprites:
            sprites = self._king_sprites

        sprites[target_position] = sprites[source_position]

        del sprites[source_position]

        self.reset(target_position)

    def move_between(self, source_position, target_position, percent): 
        pass

    def animate_move(self, position, direction):
        if position in self._men_sprites:
            sprite = self._men_sprites[position]
            sprite.image = self._animations['man_' + direction]
        if position in self._king_sprites:
            sprite = self._king_sprites[position]
            sprite.image = self._animations['king_' + direction]

    def reset(self, position):
        screen_position = (position[0] * SQUARE_SIZE, position[1] * SQUARE_SIZE)

        sprite = self.get_sprite(position)

        sprite.position = screen_position

    def reset_all(self, man_positions, king_positions):
        for sprite in self.get_all().values():
            sprite.visible = False

        self._men_sprites = {
            position: pyglet.sprite.Sprite(
                self._man_image, position[0] * SQUARE_SIZE, position[1] * SQUARE_SIZE, batch=self._batch)
            for position in man_positions
        }

        for sprite in self._men_sprites.values():
            sprite.scale = self._man_scale

        self._king_sprites = {
            position: pyglet.sprite.Sprite(
                self._king_image, position[0] * SQUARE_SIZE, position[1] * SQUARE_SIZE, batch=self._batch)
            for position in king_positions
        }

        for sprite in self._king_sprites.values():
            sprite.scale = self._king_scale

    def get_sprite(self, position):
        ''' Get a reference to a sprite for manual manipulation '''
        if position in self._men_sprites:
            return self._men_sprites[position]
        elif position in self._king_sprites:
            return self._king_sprites[position]

    def get_all(self):
        return {
            **self._men_sprites,
            **self._king_sprites
        }


def run_checkers():
    asset_path = Path('assets')

    if not asset_path.is_dir():
        sys_exit(f"Could not find assets path: {asset_path.absolute()}")

    image_assets = {
        image_file_path.stem: pyglet.image.load(image_file_path)
        for image_file_path in asset_path.glob('*.png')
    }

    window = pyglet.window.Window(WINDOW_SIZE, WINDOW_SIZE, f'not {NAME}')

    window.set_icon(image_assets['piece_red'])

    board_batch = pyglet.graphics.Batch()

    board_squares = [
        pyglet.shapes.Rectangle(
            x * SQUARE_SIZE, (y * 2 + x % 2) * SQUARE_SIZE,
            SQUARE_SIZE, SQUARE_SIZE,
            color=(96, 96, 96),
            batch=board_batch)
        for y in range(4)
        for x in range(8)
    ]

    piece_batch = pyglet.graphics.Batch()

    checkers = Checkers()

    lower_manager = CheckersPieceSpriteManager(
        SQUARE_SIZE, piece_batch, image_assets['sprite_sheet_red']
    )

    upper_manager = CheckersPieceSpriteManager(
        SQUARE_SIZE, piece_batch, image_assets['sprite_sheet_white']
    )

    possible_moves = checkers.get_possible_move_options()

    selection_batch = pyglet.graphics.Batch()

    selected_piece = None

    move_sprite_manager = SpriteManager(image_assets['selection_blue'], SQUARE_SIZE, selection_batch, 12)

    def set_pieces_with_options(possibilities):
        move_sprite_manager.set_many(possibilities)

    destination_sprite_manager = SpriteManager(image_assets['selection_green'], SQUARE_SIZE, selection_batch, 4)

    def set_move_destinations(position):
        if position is None:
            destination_sprite_manager.hide_all()
        else:
            destination_sprite_manager.set_many(possible_moves.dict[position])

    jump_destination_sprite_manager = SpriteManager(image_assets['selection_red'], SQUARE_SIZE, selection_batch, 4)

    def set_jump_destinations(position):
        if position is None:
            jump_destination_sprite_manager.hide_all()
        else:
            destination_positions = [pair[1] for pair in possible_moves.dict[position]]

            jump_destination_sprite_manager.set_many(destination_positions)

    hovered_position = None

    def set_player_move(move_options):
        nonlocal possible_moves

        possible_moves = move_options

        set_pieces_with_options(list(move_options.dict.keys()))

    def unset_player_move():
        nonlocal possible_moves

        possible_moves = None

    def set_com_move(_, move_options):
        set_pieces_with_options(list(move_options.dict.keys()))

        pyglet.clock.schedule_once(perform_com_move, 1, move_options)

    def _get_direction(source, target):
        diff_x, diff_y = target[0] - source[0], target[1] - source[1]

        if diff_x > 0:
            if diff_y > 0:
                return 'northeast'
            else:
                return 'southeast'
        else:
            if diff_y > 0:
                return 'northwest'
            else:
                return 'southwest'

    def perform_com_move(_, move_options):
        piece_position = choice(list(move_options.dict.keys()))

        destination_position = choice(list(move_options.dict[piece_position]))

        if move_options.type == 'jumps':
            destination_position = destination_position[1]

        events = checkers.make_move(piece_position, destination_position)

        sprite = upper_manager.get_sprite(piece_position)
        # sprite.image = animation

        upper_manager.animate_move(piece_position, _get_direction(piece_position, destination_position))

        pyglet.clock.schedule_once(animate_com_move, 0, sprite, piece_position, destination_position, 0)
        pyglet.clock.schedule_once(end_com_control, 0.5, move_options, events)

    def animate_com_move(dt, sprite, source_position, target_position, total):
        if total < 0.5:
            screen_position = tuple(
                (source_coord + (target_coord - source_coord) * ((total + dt) / 0.5)) * SQUARE_SIZE
                for source_coord, target_coord in zip(source_position, target_position)
            )

            sprite.position = screen_position

            pyglet.clock.schedule_once(animate_com_move, 1 / 240, sprite, source_position, target_position, total + dt)
        else:
            sprite.position = tuple(value * SQUARE_SIZE for value in target_position)

    def end_com_control(_, move_options, events):
        perform_events(move_options.side, upper_manager, lower_manager, events)

        next_move_options = checkers.get_possible_move_options()

        if next_move_options.side == move_options.side:
            set_com_move(None, next_move_options)
        else:
            set_player_move(next_move_options)

    # image_grid = get_image_grid(image_assets['sprites_red'], 16, 1)
    def perform_events(side, ally_manager, enemy_manager, events):
        for event in events:
            if event.type == 'move':
                ally_manager.move(*event.positions)
            elif event.type == 'capture':
                enemy_manager.remove(*event.positions)
            elif event.type == 'promotion':
                ally_manager.promote(*event.positions)
            elif event.type == 'won':
                declare_winner('red' if side == 'lower' else 'off white')
                return False

        return True

    def declare_winner(name):
        label = pyglet.text.Label(
            f"Team\n{name}\nwins!",
            font_name='Sans Serif',
            font_size=64,
            multiline=True, width=window.width // 2,
            x=window.width // 2, y=window.height // 2,
            anchor_x='center', anchor_y='center',
            align='center')
        
        drawables.append(label)

    @window.event
    def on_mouse_motion(x, y, dx, dy):
        nonlocal hovered_position

        if possible_moves is None:
            return

        board_x, board_y = x // SQUARE_SIZE, y // SQUARE_SIZE  

        board_position = (board_x, board_y)

        if hovered_position == board_position:
            return

        # Being on a (odd, odd) or (even, even) position means a square
        #   where a piece could be 
        in_piece_square = (board_x % 2 == board_y % 2)

        if in_piece_square and board_position in possible_moves.dict:
            if possible_moves.type == 'moves':
                set_move_destinations(board_position)
            elif possible_moves.type == 'jumps':
                set_jump_destinations(board_position)
        else:
            set_move_destinations(None)
            set_jump_destinations(None)

        hovered_position = board_position

    @window.event
    def on_mouse_press(x, y, button, modifiers):
        nonlocal selected_piece

        if possible_moves is None:
            return

        board_x, board_y = x // SQUARE_SIZE, y // SQUARE_SIZE

        if not (board_x % 2 == board_y % 2):
            return 

        board_position = (board_x, board_y)

        if board_position not in possible_moves.dict.keys():
            return

        sprite = lower_manager.get_sprite(board_position)

        selected_piece = (board_position, sprite)

    @window.event
    def on_mouse_release(x, y, buttons, modifiers):
        nonlocal selected_piece

        if possible_moves is None:
            return

        if selected_piece is None:
            return

        current_side = possible_moves.side

        board_x, board_y = x // SQUARE_SIZE, y // SQUARE_SIZE 

        if (board_x % 2 == board_y % 2):
            board_position = (board_x, board_y)

            piece_position = selected_piece[0]

            events = checkers.make_move(piece_position, board_position)

            if piece_position in possible_moves.dict and events:
                set_move_destinations(None)
                set_jump_destinations(None)
            
                perform_events(possible_moves.side, lower_manager, upper_manager, events)
                
                if not any(event.type == 'won' for event in events): 
                    next_possible_moves = checkers.get_possible_move_options()

                    # Go again!
                    if next_possible_moves.side == possible_moves.side:
                        set_player_move(next_possible_moves)
                    else:
                        unset_player_move()

                        set_com_move(None, next_possible_moves)
            else:
                lower_manager.reset(piece_position)
        else:
            lower_manager.reset(selected_piece[0])

        selected_piece = None

    @window.event
    def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
        if selected_piece is not None:
            sprite = selected_piece[1]

            sprite.x += dx
            sprite.y += dy

    drawables = [board_batch, selection_batch, piece_batch]

    @window.event
    def on_draw():
        window.clear()

        for drawable in drawables:
            drawable.draw()

    piece_positions = checkers.get_piece_layout()

    lower_manager.reset_all(piece_positions['black_pieces'], piece_positions['black_kings'])
    upper_manager.reset_all(piece_positions['white_pieces'], piece_positions['white_kings'])

    set_move_destinations(None)
    set_jump_destinations(None)

    pyglet.app.run()


def run():
    run_checkers()

