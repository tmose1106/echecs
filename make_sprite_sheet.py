from pathlib import Path
from sys import argv, exit as sys_exit

from PIL import Image


def get_dimensions(dimension_string):
    x_str, y_str = dimension_string.split(',')

    return int(x_str), int(y_str)

if __name__ == '__main__':
    usage = f'Usage: {argv[0]} image_dimensions folders...'

    try:
        dimension_string = argv[1]
    except KeyError:
        sys_exit(usage)

    try:
        image_height, image_width = get_dimensions(dimension_string)
    except ValueError:
        sys_exit(f'Invalid dimensions passed: {dimension_string}')

    directory_paths = [
        Path(dir_str)
        for dir_str in argv[2:]
    ]

    if len(directory_paths) == 0:
        sys_exit(usage)

    image_paths = [list(path.glob('*.png')) for path in directory_paths]

    directory_file_counts = [len(path_list) for path_list in image_paths]

    image_count = sum(directory_file_counts)

    if image_count == 0:
        sys_exit('No images to copy to sprite sheet')

    grid_height, grid_width = len(directory_paths), max(directory_file_counts)

    sprite_sheet_image = Image.new('RGBA', (grid_width * image_width, grid_height * image_height), (0, 0, 0, 0))

    for y, image_file_path_list in enumerate(image_paths):
        for x, image_file_path in enumerate(image_file_path_list):
            sprite_image = Image.open(image_file_path)

            sprite_sheet_image.paste(sprite_image, (x * image_width, y * image_height))

    sprite_sheet_image.save(open('sprite_sheet.png', 'wb'), format='png')

