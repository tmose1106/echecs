from pytest import mark

from echecs.bitset import Bitset, ImmutableBitset


@mark.xfail(raises=ValueError)
def test_bitset_init_fail_value_error():
    test_bitset = Bitset(4, 0b10100)


def test_bitset_int():
    assert(int(Bitset(4, 0b0010)) == 2)


def test_bitset_getitem():
    test_bitset = Bitset(4, 0b0110)

    assert(not test_bitset[0])
    assert(test_bitset[1])
    assert(test_bitset[2])
    assert(not test_bitset[3])


@mark.xfail(raises=IndexError)
def test_bitset_getitem_fail_index_error():
    test_bitset = Bitset(4, 0b0110)

    _ = test_bitset[5]


@mark.parametrize('in_value,other_value,out_value', [
    (0b0110, 0b0000, 0b0000),
    (0b0110, 0b1100, 0b0100),
])
def test_bitset_and(in_value, other_value, out_value):
    test_bitset, other_bitset = Bitset(4, in_value), Bitset(4, other_value)

    result_bitset = test_bitset & other_bitset

    assert(int(result_bitset) == out_value)


def test_bitset_invert():
    assert(int(~Bitset(4, 0b0110)) == 0b1001)


def test_bitset_iter():
    test_bitset = Bitset(4, 0b0110)

    assert(tuple(test_bitset) == (False, True, True, False))


def test_bitset_set(): 
    test_bitset = Bitset(4, 0b0000)

    assert(not test_bitset[2])

    test_bitset.set(2)

    assert(test_bitset[2])


def test_bitset_unset():
    test_bitset = Bitset(4, 0b0100)

    assert(test_bitset[2])

    test_bitset.unset(2)

    assert(not test_bitset[2])


def test_bitset_any():
    test_bitset = Bitset(4, 0b0000)

    assert(not test_bitset.any())

    test_bitset.set(1)

    assert(test_bitset.any())


@mark.parametrize('in_value,rotate,out_value', [
    (0b0110, 1, 0b1100),
    (0b0110, 2, 0b1001),
    (0b0110, -3, 0b1100),
])
def test_bitset_rotate(in_value, rotate, out_value):
    # 0110
    test_bitset = Bitset(4, in_value)

    # 1100
    test_bitset.rotate(rotate)

    assert(int(test_bitset) == out_value)

